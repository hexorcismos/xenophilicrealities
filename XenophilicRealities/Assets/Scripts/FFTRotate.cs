﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FFTRotate : MonoBehaviour
{
    public GameObject _sampleXenoPrefab;
    public int band;
    //GameObject[] _sampleXeno = new GameObject[512];
    public float _maxScale;
    // Start is called before the first frame update
    void Start()
    {

    }


    // Update is called once per frame
    void Update()
    {
        Vector3 scale = new Vector3((AudioPeer._samples[band] * _maxScale) + 0.1f, (AudioPeer._samples[band+1] * _maxScale) + 0.1f, (AudioPeer._samples[band+2] * _maxScale) + 0.1f);


        if (_sampleXenoPrefab != null)
        {
            //_sampleXenoPrefab.transform.localScale = scale;
            _sampleXenoPrefab.transform.localRotation = new Quaternion(scale.x, scale.y, scale.z, 1f);

        }

    }
}

