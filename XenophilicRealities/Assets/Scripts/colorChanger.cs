﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colorChanger : MonoBehaviour
{
    public GameObject _xeno;
    Color lerpedColor = Color.white;
    public Material shader;
    public int band;
    public float _maxScale;
    // Start is called before the first frame update
    void Start()
    {
        //Fetch the Renderer from the GameObject
        //Renderer rend = GetComponent<Renderer>();

    }


    // Update is called once per frame
    void Update()
    {
        Vector3 scale = new Vector3((AudioPeer._samples[band] * _maxScale) + 0.05f, (AudioPeer._samples[band] * _maxScale) + 0.05f, (AudioPeer._samples[band] * _maxScale) + 0.05f);
        //Debug.Log(scale.x);

        if (_xeno != null)
        { 
            if (scale.x > 0.5f)
            {
                //Renderer rend = GetComponent<Renderer>();
                //Set the main Color of the Material to green
                //rend.material.shader = Shader.Find("_Color");
                //rend.material.SetColor("_Color", Color.gray);
                lerpedColor = Color.Lerp(Color.green, Color.blue, scale.x);
                shader.SetColor("_Color", lerpedColor);
            }
            if (scale.x > 0f)
            {
                lerpedColor = Color.Lerp(Color.green, lerpedColor, scale.y);        
                shader.SetColor("_Color", lerpedColor);
            }
            else
            {
                //Renderer rend = GetComponent<Renderer>();
                //Set the main Color of the Material to green
                //rend.material.shader = Shader.Find("_Color");
                //rend.material.SetColor("_Color", Color.green);
                //shader.SetColor("_Color", Color.black);
                lerpedColor = Color.Lerp(Color.red, Color.green, scale.x);        
                shader.SetColor("_Color", lerpedColor);
            }


            //Find the Specular shader and change its Color to red
            //rend.material.shader = Shader.Find("Specular");
            //rend.material.SetColor("_SpecColor", Color.red);

            //shader.Set

            shader.SetFloat("_Metallic", scale.x);
           //_shader.Material
            //shader.SetFloat("_Distance", scale.y);
           // _xeno.transform.localScale = scale;
           // _xeno.transform.rotation = new Quaternion(scale.x * 90f, scale.y * 90f, 0f, 1f);
        }
    }
}
