﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralSynthControl : MonoBehaviour
{
    Hv_TeleportElastic4_AudioLib _xenoSynth;
    public GameObject _sampleXenoPrefab;
    public Vector3 _offset;

    // Start is called before the first frame update
    void Start()
    {
        _xenoSynth = GetComponent<Hv_TeleportElastic4_AudioLib>();
        _xenoSynth.SetFloatParameter(Hv_TeleportElastic4_AudioLib.Parameter.Gain, 0.75f);
        _xenoSynth.SetFloatParameter(Hv_TeleportElastic4_AudioLib.Parameter.Reverbgain, 0.5f);
        _xenoSynth.SetFloatParameter(Hv_TeleportElastic4_AudioLib.Parameter.Sweepdecay, 0.9f);
    }

    // Update is called once per frame
    void Update()
    {
        _xenoSynth.SetFloatParameter(Hv_TeleportElastic4_AudioLib.Parameter.Laserpitch, _xenoSynth.transform.localScale.x + _offset.x);
        _xenoSynth.SetFloatParameter(Hv_TeleportElastic4_AudioLib.Parameter.Laserlfo, _xenoSynth.transform.localScale.y + _offset.y);
        _xenoSynth.SetFloatParameter(Hv_TeleportElastic4_AudioLib.Parameter.Sweep, _xenoSynth.transform.localScale.z + _offset.z);
    }
}
