﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiate512Cubes : MonoBehaviour
{
    public GameObject _sampleXenoPrefab;
    GameObject[] _sampleXeno = new GameObject[512];
    public float _maxScale;
    // Start is called before the first frame update
    void Start()
    {

        for (int i = 0; i < 512; i++)
        {

            GameObject _instanceSampleXeno = (GameObject)Instantiate(_sampleXenoPrefab);
            _instanceSampleXeno.transform.position = this.transform.position;
            _instanceSampleXeno.transform.parent = this.transform;
            _instanceSampleXeno.name = "SamplePrefab" + i;
            this.transform.eulerAngles = new Vector3(0, 0.70315f * i, 0);
            _instanceSampleXeno.transform.position = Vector3.forward * 100;
            _sampleXeno[i] = _instanceSampleXeno;

        }
    }


    // Update is called once per frame
    void Update() {
        for (int i = 0; i < 512; i++) {
            if (_sampleXeno != null)
            {
              _sampleXeno[i].transform.localScale = new Vector3(1, (AudioPeer._samples[i] * _maxScale) + 2, 1);
            }
        }
    }
}    


      


