﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FFTAmp : MonoBehaviour
{
    public GameObject _sampleXenoPrefab;
    //public Material _material;
    public int band;
    //GameObject[] _sampleXeno = new GameObject[512];
    public float _maxScale;
    // Start is called before the first frame update
    void Start()
    {
       ///_material = GetComponentInChildren<Material>();
    }


    // Update is called once per frame
    void Update()
    {   

        Vector3 scale = new Vector3((AudioPeer._samples[band] * _maxScale) + 0.05f, (AudioPeer._samples[band] * _maxScale) + 0.05f, (AudioPeer._samples[band] * _maxScale) + 0.05f);


            if (_sampleXenoPrefab != null)
            {
            _sampleXenoPrefab.transform.localScale = scale;
            _sampleXenoPrefab.transform.rotation = new Quaternion(scale.x * 90f, scale.y * 90f,0f, 1f);

               
        }
        
    }
}
