#XenophilicRealities

An XR generative music playground [Work in Progress]

Last update 11.06.2019

Features:

• Create initial Xenophil types:

Sample based (loops) Procedural (instruments) Live sound input (loops)

• Audio reactivity

FFT first mapped to amplitude Shader colors to component Vertex shaders changing?

• Interaction:

Put in planes Different sounds correlating to color/shapes Create diversity in the shapes/movements

Questions for the future:

Multiplayer development Branding/marketing? Special figures?

